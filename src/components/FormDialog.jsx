import React, {Fragment, Component} from 'react';
import Dialog, {DialogContent, DialogActions} from 'material-ui/Dialog';
import Button from 'material-ui/Button';
import Form from './Form';

export default class DialogExampleSimple extends Component {
  constructor(props) {
    super()
    this.state = {
      open: props.open || false,
      formValid: false
    }
  }

  handleOpen = () => {
    this.setState({open: true});
  }

  handleClose = () => {
    this.setState({open: false});
  }

  render() {
    return (
      <Fragment>
        <Button variant="raised" onClick={this.handleOpen}>{this.props.label || "Form button"}</Button>
        <Dialog
          open={this.state.open}
          aria-labelledby="form-dialog-title"
        >
          <DialogContent>
            <Form species={this.props.species} formValidChange={(state) => this.setState({formValid: state})}/>
          </DialogContent>
          <DialogActions>
            <Button
              variant="raised"
              onClick={this.handleClose}
            >Cancel</Button>,
            <Button
              variant="raised"
              disabled={!this.state.formValid}
              onClick={this.handleClose}
            >Submit</Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}