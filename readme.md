# Vincit 2018
Assignment for Vincit 2018 summer job application. Uses <https://github.com/Vincit/summer-2018> for backend and data fetching.

## Usage (for now)
Run 
```
npm install
npm start
```
`npm start` will open a browser window for live preview of the project. It reloads the page every time a component file that the app depends on is saved.

Backend installation instructions coming later...